package com.lduparc.wizardpager.lib.model;

import com.lduparc.wizardpager.lib.ui.CustomerInfoFragment;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import java.util.ArrayList;

/**
 * A page asking for a name and an email.
 */
public class CustomerInfoPage extends Page {

    public static final String PAGE_KEY = "CustomerData:key";
    public static final String QUESTION_DATA_KEY = "question:key";

    public CustomerInfoPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
        setParentKey(PAGE_KEY);
    }


    @Override
    public Fragment createFragmentWithLayouts(int container, int item) {
        return CustomerInfoFragment.create(getKey(), container, item, null);
    }

    @Override
    public Fragment createFragmentWithLayoutsContent(int container, int item, String content) {
        return createFragmentWithLayouts(container, item);
    }

    @Override
    public Fragment createFragment() {
        return CustomerInfoFragment.create(getKey(), -1, -1, null);
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem("Your content", mData.getString(QUESTION_DATA_KEY), getKey(), -1));
    }

    @Override
    public boolean isCompleted() {
        return !TextUtils.isEmpty(mData.getString(QUESTION_DATA_KEY));
    }
}
