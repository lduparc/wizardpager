package com.lduparc.wizardpager.lib.model;

public interface ModelCallbacks {
    void onPageDataChanged(Page page);
    void onPageTreeChanged();
}
