package com.lduparc.wizardpager.lib.model;

import com.lduparc.wizardpager.lib.ui.SingleFixedChoiceFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * A page offering the user a number of mutually exclusive choices.
 */
public class SingleFixedChoicePage extends Page {

    protected ArrayList<String> mChoices = new ArrayList<String>();

    public SingleFixedChoicePage(ModelCallbacks callbacks, String title, String key) {
        super(callbacks, title);
        Log.d("trololo", "SingleFixedChoicePage key : "+ key);
        setParentKey(key);
    }

    @Override
    public Fragment createFragment() {
        return SingleFixedChoiceFragment.create(getKey());
    }

    @Override
    public Fragment createFragmentWithLayouts(int container, int item) {
        return SingleFixedChoiceFragment.create(getKey(), container, item);
    }

    @Override
    public Fragment createFragmentWithLayoutsContent(int container, int item, String content) {
        return createFragmentWithLayouts(container, item);
    }

    public int getOptionIndex(String value) {return mChoices.indexOf(value);}

    public String getOptionAt(int position) {
        return mChoices.get(position);
    }

    public int getOptionCount() {
        return mChoices.size();
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(getTitle(), mData.getBundle(SIMPLE_DATA_KEY), getKey()));
    }

    @Override
    public boolean isCompleted() {
        return !TextUtils.isEmpty(mData.getString(SIMPLE_DATA_KEY));
    }

    public SingleFixedChoicePage setChoices(String... choices) {
        mChoices.addAll(Arrays.asList(choices));
        return this;
    }

    public SingleFixedChoicePage setChoices(List<String> choices) {
        mChoices.addAll(choices);
        return this;
    }

}
