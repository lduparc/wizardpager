package com.lduparc.wizardpager.lib.model;

import com.lduparc.wizardpager.lib.ui.MultipleChoiceFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

/**
 * A page offering the user a number of non-mutually exclusive choices.
 */
public class MultipleFixedChoicePage extends SingleFixedChoicePage {
    public MultipleFixedChoicePage(ModelCallbacks callbacks, String title, String key) {
        super(callbacks, title, key);
    }

    @Override
    public Fragment createFragmentWithLayouts(int container, int item) {
        return createFragment();
    }

    @Override
    public Fragment createFragmentWithLayoutsContent(int container, int item, String content) {
        return createFragment();
    }

    @Override
    public Fragment createFragment() {
        return MultipleChoiceFragment.create(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        StringBuilder sb = new StringBuilder();

        ArrayList<String> selections = mData.getStringArrayList(Page.SIMPLE_DATA_KEY);
        if (selections != null && selections.size() > 0) {
            for (String selection : selections) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(selection);
            }
        }

        Bundle data = new Bundle();
        data.putString("value", sb.toString());
        dest.add(new ReviewItem(getTitle(), data, getKey()));
    }

    @Override
    public boolean isCompleted() {
        ArrayList<String> selections = mData.getStringArrayList(Page.SIMPLE_DATA_KEY);
        return selections != null && selections.size() > 0;
    }
}
