package com.lduparc.wizardpager.lib.ui;

import com.lduparc.wizardpager.lib.model.Page;

public interface PageFragmentCallbacks {
    Page onGetPage(String key);
}
