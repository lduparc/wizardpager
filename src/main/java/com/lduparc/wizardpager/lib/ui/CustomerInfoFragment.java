package com.lduparc.wizardpager.lib.ui;

import com.lduparc.wizardpager.lib.R;
import com.lduparc.wizardpager.lib.model.CustomerInfoPage;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

public class CustomerInfoFragment extends Fragment {

    private static final String ARG_KEY = "key";

    private static final String ARG_CONTENT_KEY = "content:key";

    private static final String ARG_RES_KEY = "res:key";
    private static final String ARG_CONTAINER_RES_KEY = "container:res:key";

    private PageFragmentCallbacks mCallbacks;

    private String mKey;

    private CustomerInfoPage mPage;

    private TextView mContentView;

    public static CustomerInfoFragment create(String key, int container, int item, String content) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        if (content != null) {
            args.putString(ARG_CONTENT_KEY, content);
        }
        args.putInt(ARG_CONTAINER_RES_KEY, container);
        args.putInt(ARG_RES_KEY, item);
        CustomerInfoFragment fragment = new CustomerInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public CustomerInfoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (CustomerInfoPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        Bundle args = getArguments();
        int res = args.getInt(ARG_RES_KEY, -1);
        int layout = res == -1 ? R.layout.fragment_page_customer_info : res;
        View rootView = inflater.inflate(layout, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mContentView = ((TextView) rootView.findViewById(R.id.question_content));
        mContentView.setText(mPage.getData().getString(CustomerInfoPage.QUESTION_DATA_KEY));

        if (args.containsKey(ARG_CONTENT_KEY)) {
            mContentView.setText(args.getString(ARG_CONTENT_KEY));
        }
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContentView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1,
                    int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mPage.getData().putString(CustomerInfoPage.QUESTION_DATA_KEY,
                        (editable != null) ? editable.toString() : null);
                mPage.notifyDataChanged();
            }
        });
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        // In a future update to the support library, this should override setUserVisibleHint
        // instead of setMenuVisibility.
        if (mContentView != null) {
            InputMethodManager imm = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (!menuVisible) {
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        }
    }
}
