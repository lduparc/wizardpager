package com.lduparc.wizardpager.lib.ui;

import com.lduparc.wizardpager.lib.R;
import com.lduparc.wizardpager.lib.model.Page;
import com.lduparc.wizardpager.lib.model.SingleFixedChoicePage;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SingleFixedChoiceFragment extends ListFragment {

    private static final String ARG_KEY = "key";

    private static final String ARG_RES_KEY = "res_key";

    private static final String ARG_CONTAINER_RES_KEY = "container_res_key";

    private PageFragmentCallbacks mCallbacks;

    private List<String> mChoices;

    private String mKey;

    private Page mPage;

    public static SingleFixedChoiceFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        args.putInt(ARG_CONTAINER_RES_KEY, -1);
        args.putInt(ARG_RES_KEY, -1);
        SingleFixedChoiceFragment fragment = new SingleFixedChoiceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static SingleFixedChoiceFragment create(String key, int containerLayout, int resLayout) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        args.putInt(ARG_CONTAINER_RES_KEY, containerLayout);
        args.putInt(ARG_RES_KEY, resLayout);
        SingleFixedChoiceFragment fragment = new SingleFixedChoiceFragment();

        fragment.setArguments(args);
        return fragment;
    }

    public SingleFixedChoiceFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = mCallbacks.onGetPage(mKey);

        SingleFixedChoicePage fixedChoicePage = (SingleFixedChoicePage) mPage;
        mChoices = new ArrayList<String>();
        for (int i = 0; i < fixedChoicePage.getOptionCount(); i++) {
            mChoices.add(fixedChoicePage.getOptionAt(i));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        Bundle args = getArguments();
        final int containerResLayout =
                args != null && getArguments().getInt(ARG_CONTAINER_RES_KEY) != -1 ? args
                        .getInt(ARG_CONTAINER_RES_KEY) : R.layout.fragment_page;
        final int itemResLayout = args != null && getArguments().getInt(ARG_RES_KEY) != -1 ? args
                .getInt(ARG_RES_KEY) : android.R.layout.simple_list_item_single_choice;

        View rootView = inflater.inflate(containerResLayout, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        final ListView listView = (ListView) rootView.findViewById(android.R.id.list);
        setListAdapter(new ArrayAdapter<String>(getActivity(),
                itemResLayout,
                android.R.id.text1,
                mChoices));
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        // Pre-select currently selected item.
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                String selection = mPage.getData().getString(Page.SIMPLE_DATA_KEY);
                for (int i = 0; i < mChoices.size(); i++) {
                    if (mChoices.get(i).equals(selection)) {
                        listView.setItemChecked(i, true);
                        break;
                    }
                }
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        mPage.getData().putString(Page.SIMPLE_DATA_KEY, getListAdapter().getItem(position).toString());
        mPage.getData().putInt(Page.SIMPLE_POSITION_KEY, position == getListAdapter().getCount() - 1 ? -1 : position);
        mPage.notifyDataChanged();
    }
}
